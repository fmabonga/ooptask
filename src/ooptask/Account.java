package ooptask;

import java.util.Date;

public class Account{
	protected int id;
	protected double balance;
	protected Date dateCreated;
		
	public Account(int custID,double initialBalance){
		id = custID;
		balance = initialBalance;
	}
	protected int getId(){
		return this.id;
	}
	protected double getBalance(){
		return this.balance;
	}
	protected Date getDateCreated(){
		return this.dateCreated;
	}
	protected void setId(int custID){
		this.id = custID;
	}
	protected void setBalance(double newBalance){
		this.balance = newBalance;
	}
	protected void withdraw(double amount){
		this.balance -= amount;
	}
	protected void deposit(double amount){
		this.balance += amount;
	}
}