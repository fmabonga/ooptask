package ooptask;

public class SavingsAccount extends Account{
	private double annualInterestRate;
	public SavingsAccount(int custID, double initialBalance) {
		super(custID, initialBalance);
	}
	private double getMonthlyInterestRate(){
		return (this.annualInterestRate/12)/100;
	}
	private double getMonthlyInterest(){
		return this.balance * this.getMonthlyInterestRate();
	}
		
}