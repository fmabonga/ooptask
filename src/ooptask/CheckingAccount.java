package ooptask;
public class CheckingAccount extends Account{
	private double overdraft;
	public CheckingAccount(int custID, double initialBalance) {
		super(custID, initialBalance);
	}
	private double getOverdraft(){
		return this.overdraft;
	}
}