package ooptask;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OopTask {
	private static List<Account> custAccounts;
	
	public static void main(String args[]){
		custAccounts = new ArrayList<Account>();
		for(int x=1;x<=10;x++){
			if(x<=5){
				custAccounts.add(new SavingsAccount(x,1000));
			}else{
				custAccounts.add(new CheckingAccount(x,1000));
			}
			
		}
		enterId();
	}
	public static void enterId(){
		Scanner inputObj = new Scanner(System.in);
		boolean run = true;
		while(run){
			System.out.println("Please Enter the account ID: ");
			int id = inputObj.nextInt();
			for(int i=0;i<custAccounts.size();i++){
				if(id == custAccounts.get(i).getId()){
					run = false;
					mainMenu(custAccounts.get(i));
					break;
				}
			}
		}
		inputObj.close();
	}
	
	public static void mainMenu(Account acc){
		System.out.println("Main menu\n" +
							 "1. check balance\n" +
				             "2. withdraw\n" +
							 "3. deposit\n" +
				             "4. exit\n");
		Scanner optionObj = new Scanner(System.in);
		boolean run = true;
			while(run){
				System.out.println("Please Enter menu option: ");
				int menuOption = optionObj.nextInt();
				switch(menuOption){
				case 1:
					System.out.println("The account balance for account "+acc.getId()+" is :"+acc.getBalance());
					break;
				case 2:
					Scanner getWithdrawAmount = new Scanner(System.in);
					System.out.println("Please Enter the amount to withdraw: ");
					double withdrawAmount = getWithdrawAmount.nextDouble();
					acc.withdraw(withdrawAmount);
					getWithdrawAmount.close();
					run =false;
					break;
				case 3:
					Scanner getDepositAmount = new Scanner(System.in);
					System.out.println("Please Enter the amount to deposit: ");
					double depositAmount = getDepositAmount.nextDouble();
					acc.withdraw( depositAmount);
					System.out.println("Amount "+ depositAmount+" deposited ");
					getDepositAmount.close();
					break;
				case 4:
					enterId();
					run = false;
					break;
				default:
					System.out.println("You entered an incorrect option, please try again");
				}
			}
			optionObj.close();
	}
}
